package twitterData;

public class TestClass {
		private int x;
		private int y;
		private int sumxy;
		
		TestClass(int x, int y){
			this.x = x;
			this.y = y;
		}
		
		private int getSum(){
			this.sumxy = this.x + this.y;
			return this.sumxy;
		}
		
	public static void main(String[] args) {
		TestClass o = new TestClass(6, 7);
		System.out.println(o.getSum());
		System.out.println("\n".length());
	}

}
