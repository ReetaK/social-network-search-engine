package twitterData;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import twitter4j.GeoLocation;
import twitter4j.Status;
import twitter4j.User;

public class TweetProcessor {

	private final int MAX_CHAR_CAPACITY = 10485760; // 10 MB
	private final int MAX_SET_SIZE = 150000; // 292 KB
	private StringBuilder sb = new StringBuilder(MAX_CHAR_CAPACITY);
	private int fileCount = 300;
	private Set<Long> ids = new HashSet<Long>(MAX_SET_SIZE);
	ObjectMapper mapper = new ObjectMapper();
	private final String path;

	TweetProcessor(String path) {
		this.path = path;
	}

	void processTweet(Status status) {
		if (ids.contains(status.getId()) || null == status.getGeoLocation())
			return;

		TweetGetSet tweet = findTweetClass(status);

		try {
			sb.append(mapper.writeValueAsString(tweet));
			sb.append("\n");

			ids.add(tweet.getID());

			if (sb.length() >= MAX_CHAR_CAPACITY) {

				try (BufferedWriter bfw = new BufferedWriter(
						new FileWriter(new File(path + "TweetPreProcess" + (++fileCount) + ".txt")))) {
					bfw.write(sb.toString());
					System.out.println("Written File:" + fileCount);
				} catch (IOException e) {
					e.printStackTrace();
				}

				sb.setLength(0);
			}
			if (ids.size() >= MAX_SET_SIZE)
				ids = new HashSet<Long>(MAX_SET_SIZE);
		} catch (JsonProcessingException e1) {
			e1.printStackTrace();
		}

	}

	TweetGetSet findTweetClass(Status status) {
		TweetGetSet retTweet = new TweetGetSet();

		retTweet.setID(status.getId());
		retTweet.setTime(status.getCreatedAt());

		GeoLocation glocation = status.getGeoLocation();
		double[] location;
		if (null == glocation)
			location = null;
		else {
			location = new double[2];
			location[0] = glocation.getLatitude();
			location[1] = glocation.getLongitude();
		}
		retTweet.setLocation(location);

		retTweet.setText(status.getText());
		retTweet.setLang(status.getLang());

		User user = status.getUser();
		retTweet.setUser((null == user) ? null : user.getName());
		retTweet.setScreenName((null == user) ? null : user.getScreenName());

		retTweet.setSource(status.getSource());
		retTweet.setContributors(status.getContributors());
		retTweet.setWithheldCountries(status.getWithheldInCountries());
		retTweet.setPlace(status.getPlace().getName());
		retTweet.setIsFavorite(status.isFavorited());
		retTweet.setFavoriteCount(status.getFavoriteCount());
		retTweet.setIsSensitive(status.isPossiblySensitive());
		retTweet.setIsRetweet(status.isRetweet());
		retTweet.setRetweetCount(status.getRetweetCount());
		retTweet.setIsTruncated(status.isTruncated());
		retTweet.setAccessLevel(status.getAccessLevel());

		retTweet.setUrlEntity(URLEntityImpl.getURLEntityImpl(status.getURLEntities()));

		/*
		 * Getting URLs and title of URLs is a costly task here so will be done
		 * in postprocessing
		 */
		retTweet.setUrls(null);
		retTweet.setTitleUrls(null);

		retTweet.setHashtags(HashtagEntityImpl.getHashtagEntityImpl(status.getHashtagEntities()));

		return retTweet;
	}
}
