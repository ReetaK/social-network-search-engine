package twitterData;

import twitter4j.FilterQuery;
import twitter4j.StallWarning;
import twitter4j.Status;
import twitter4j.StatusDeletionNotice;
import twitter4j.StatusListener;
import twitter4j.TwitterException;
import twitter4j.TwitterStream;
import twitter4j.TwitterStreamFactory;
import twitter4j.conf.Configuration;
import twitter4j.conf.ConfigurationBuilder;

public class TweetListener implements StatusListener{

	private final TweetProcessor processor;
	
	TweetListener(String path){
		processor = new TweetProcessor(path);
	}
	
	@Override
	public void onException(Exception arg0) {
		
	}

	@Override
	public void onDeletionNotice(StatusDeletionNotice arg0) {
		
	}

	@Override
	public void onScrubGeo(long arg0, long arg1) {
		
	}

	@Override
	public void onStallWarning(StallWarning warning) {
		System.out.println("Stall Warning: "+ warning);	
	}

	@Override
	public void onStatus(Status status) {
		processor.processTweet(status);
	}

	@Override
	public void onTrackLimitationNotice(int arg0) {
		
	}
	
	public static void main(String[] args) throws TwitterException{
		ConfigurationBuilder confBuilder = new twitter4j.conf.ConfigurationBuilder();
		confBuilder.setOAuthConsumerKey(args[0]); 
		confBuilder.setOAuthConsumerSecret(args[1]); 
		confBuilder.setOAuthAccessToken(args[2]); 
		confBuilder.setOAuthAccessTokenSecret(args[3]); 
		Configuration config = confBuilder.build();
		String path = args[4];
		TwitterStream twitterStream = new TwitterStreamFactory(config).getInstance();
		TweetListener listener = new TweetListener(path); 
		twitterStream.addListener(listener);
		
		double[][] boundingbox = {{-124.47,24.0},{-66.56,49.3843}}; 		// Whole US
		
		FilterQuery filter = new FilterQuery(); 
		filter.locations(boundingbox);
	
		twitterStream.filter(filter);
	}
}
