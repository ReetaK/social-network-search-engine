package twitterData;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import org.jsoup.Jsoup;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import twitter4j.URLEntity;

public class TweetUrl_Title {

	//private final int MAX_CHAR_CAPACITY = 10485760; // 10 MB
	private StringBuilder sb = new StringBuilder();
	private int fileCount = 1;
	private ObjectMapper mapper = new ObjectMapper();

	private void processUrl() {

		// InputStream instream = TweetGetSet.class.getResourceAsStream(
		// "/Users/reetas/RS_Work/Fall16/IR/Project/Data/TweetPreProcess"+fileCount+".txt");
		// byte[] jsonInStr =
		// Files.readAllBytes(Paths.get("/Users/reetas/RS_Work/Fall16/IR/Project/Data/TweetPreProcess"+fileCount+".txt"));
		// byte[] jsonInStr =
		// Files.readAllBytes(Paths.get("/Users/reetas/RS_Work/Fall16/IR/Project/Data/TweetPreProcess1.txt"));
		try (BufferedReader bfr = new BufferedReader(
				new FileReader("/Users/reetas/RS_Work/Fall16/IR/Project/Data/TweetPreProcess" + fileCount + ".txt"))) {
			// TweetGetSet tweet = mapper.readValue(jsonInStr,
			// TweetGetSet.class);
			JsonParser jp = new JsonFactory().createParser(bfr);
			jp.setCodec(mapper);
			jp.nextToken();
			while (jp.hasCurrentToken()) {
				
				TweetGetSet tweet = jp.readValueAs(TweetGetSet.class);
				jp.nextToken();
				//System.out.println("ID: " + tweet.getID());
				URLEntity[] urlEntity = tweet.getURLEntity();
				
				int numUrls = (null == urlEntity) ? 0 : urlEntity.length;
				String[] urls = new String[numUrls];
				String[] titleUrls = new String[numUrls];
				for (int i = 0; i < numUrls; ++i) {
					urls[i] = (null==urlEntity[i]) ? null : urlEntity[i].getURL();
					try {
						titleUrls[i] = (null == urls[i]) ? null : Jsoup.connect(urls[i]).get().title();
					} catch (IOException e) {
						titleUrls[i] = null;
						System.err.println("Title of the unauthorized url: " + urls[i] + " has been set as Null");
					}
				}
				tweet.setUrls(urls);
				tweet.setTitleUrls(titleUrls);

				sb.append(mapper.writeValueAsString(tweet));
				sb.append("\n");

				// if (sb.length() >= MAX_CHAR_CAPACITY) {

				// sb.setLength(0);
				// }
			}

		} catch (JsonParseException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		try (BufferedWriter bfw = new BufferedWriter(new FileWriter(new File(
				"/Users/reetas/RS_Work/Fall16/IR/Project/Data/Processed/TweetProcessed" + fileCount + ".json")))) {
			// mapper.configure(SerializationFeature.INDENT_OUTPUT, true);

			// writing to console, can write to any output stream such as file
			// StringWriter stringEmp = new StringWriter();
			// mapper.writeValue(bfw, tweet);
			
			// bfw.write(stringEmp);
			bfw.write(sb.toString());
			System.out.println("Written Processed File:" + fileCount);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		TweetUrl_Title urlProcess = new TweetUrl_Title();
			urlProcess.processUrl();
	}

}
