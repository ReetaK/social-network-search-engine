package twitterData;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import org.jsoup.Jsoup;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import twitter4j.URLEntity;

public class TweetUrl_TitleMultiThread {

	static class ProcessURL implements Runnable {

		private AtomicInteger ai;
		private List<TweetGetSet> tweetList;

		public ProcessURL(AtomicInteger ai, List<TweetGetSet> tweetList) {
			this.ai = ai;
			this.tweetList = tweetList;
		}

		@Override
		public void run() {
			for (int curr = ai.getAndIncrement(); curr < tweetList.size(); curr = ai.getAndIncrement()) {
				TweetGetSet tweet = tweetList.get(curr);
				URLEntity[] urlEntity = tweet.getURLEntity();
				int numUrls = (null == urlEntity) ? 0 : urlEntity.length;
				String[] urls = new String[numUrls];
				String[] titleUrls = new String[numUrls];
				for (int i = 0; i < numUrls; ++i) {
					urls[i] = (null == urlEntity[i]) ? null : urlEntity[i].getURL();
					try {
						titleUrls[i] = (null == urls[i]) ? null : Jsoup.connect(urls[i]).get().title();
					} catch (IOException e) {
						titleUrls[i] = null;
						System.err.println("Title of the unauthorized url: " + urls[i] + " has been set as Null");
					}
				}
				tweet.setUrls(urls);
				tweet.setTitleUrls(titleUrls);
				tweetList.set(curr, tweet);
			}
		}

	}

	public static void main(String[] args) {
		final int start = Integer.parseInt(args[0]);
		final int end = Integer.parseInt(args[1]);
		final String inPath = args[2];
		final String outPath = args[3];

		for (int fileCount = start; fileCount <= end; ++fileCount) {
			ObjectMapper mapper = new ObjectMapper();
			List<TweetGetSet> tweetList = new ArrayList<TweetGetSet>();

			try (BufferedReader bfr = new BufferedReader(new FileReader(
					inPath + "TweetPreProcess"+fileCount + ".txt"))) {
				JsonParser jp = new JsonFactory().createParser(bfr);
				jp.setCodec(mapper);
				jp.nextToken();
				while (jp.hasCurrentToken()) {
					TweetGetSet tweet = jp.readValueAs(TweetGetSet.class);
					tweetList.add(tweet);
					jp.nextToken();
				}

				AtomicInteger ai = new AtomicInteger(0);
				ExecutorService exec = Executors.newFixedThreadPool(64);

				for (int i = 0; i < 64; ++i)
					exec.execute(new ProcessURL(ai, tweetList));
				exec.shutdown();
				int countException = 0;
				while (true) {
					boolean breakout = false;
					try {
						breakout = exec.awaitTermination(2, TimeUnit.MINUTES);
					} catch (InterruptedException e) {
						e.printStackTrace();
						countException++;
						if (countException == 5)
							break;
					}
					if (breakout)
						break;
				}

				StringBuilder sb = new StringBuilder();
				for (TweetGetSet tweet : tweetList) {
					try {
						sb.append(mapper.writeValueAsString(tweet));
					} catch (JsonProcessingException e) {
						e.printStackTrace();
					}
					sb.append("\n");
				}
				try (BufferedWriter bfw = new BufferedWriter(new FileWriter(
						new File(outPath+"TweetMultiThreadProcess"
								+ fileCount + ".json")))) {

					bfw.write(sb.toString());
					System.out.println("Written Processed File:" + fileCount);
				} catch (IOException e) {
					e.printStackTrace();
				}
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

}
